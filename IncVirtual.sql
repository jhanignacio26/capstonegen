USE [master]
GO
/****** Object:  Database [IncVirtual]    Script Date: 31/05/2022 05:29:56 JC ******/
CREATE DATABASE [IncVirtual]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'IncVirtual', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\IncVirtual.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'IncVirtual_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.SQLEXPRESS\MSSQL\DATA\IncVirtual_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [IncVirtual] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [IncVirtual].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [IncVirtual] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [IncVirtual] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [IncVirtual] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [IncVirtual] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [IncVirtual] SET ARITHABORT OFF 
GO
ALTER DATABASE [IncVirtual] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [IncVirtual] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [IncVirtual] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [IncVirtual] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [IncVirtual] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [IncVirtual] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [IncVirtual] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [IncVirtual] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [IncVirtual] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [IncVirtual] SET  DISABLE_BROKER 
GO
ALTER DATABASE [IncVirtual] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [IncVirtual] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [IncVirtual] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [IncVirtual] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [IncVirtual] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [IncVirtual] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [IncVirtual] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [IncVirtual] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [IncVirtual] SET  MULTI_USER 
GO
ALTER DATABASE [IncVirtual] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [IncVirtual] SET DB_CHAINING OFF 
GO
ALTER DATABASE [IncVirtual] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [IncVirtual] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [IncVirtual] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [IncVirtual] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [IncVirtual] SET QUERY_STORE = OFF
GO
USE [IncVirtual]
GO
/****** Object:  Table [dbo].[Aprendizaje]    Script Date: 31/05/2022 05:29:56 JC ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Aprendizaje](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdCurso] [int] NULL,
	[Descripcion] [nvarchar](200) NULL,
 CONSTRAINT [PK_Aprendizaje] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Categoria]    Script Date: 31/05/2022 05:29:56 JC ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categoria](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](100) NULL,
 CONSTRAINT [PK_Categoria] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cesta]    Script Date: 31/05/2022 05:29:56 JC ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cesta](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdUser] [int] NULL,
	[IdCurso] [int] NULL,
	[Pagado] [bit] NULL,
 CONSTRAINT [PK_CursoUsuario] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Clases]    Script Date: 31/05/2022 05:29:56 JC ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Clases](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdModulo] [int] NULL,
	[Nombre] [nvarchar](200) NULL,
	[Video] [nvarchar](500) NULL,
	[Descripcion] [nvarchar](2500) NULL,
 CONSTRAINT [PK_Clases] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ComentarioCurso]    Script Date: 31/05/2022 05:29:56 JC ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ComentarioCurso](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdCurso] [int] NULL,
	[IdUsuario] [int] NULL,
	[Comentario] [nvarchar](1000) NULL,
 CONSTRAINT [PK_ComentarioCurso] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Curso]    Script Date: 31/05/2022 05:29:56 JC ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Curso](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Imagen] [nvarchar](100) NULL,
	[Nombre] [nvarchar](50) NULL,
	[Detalle] [nvarchar](2500) NULL,
	[Descripcion] [nvarchar](2500) NULL,
	[Precio] [decimal](18, 2) NULL,
	[IdDocente] [int] NULL,
	[IdCategoria] [int] NULL,
	[Video] [nvarchar](250) NULL,
	[Estado] [bit] NULL,
 CONSTRAINT [PK_Cursos] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CursoUsuario]    Script Date: 31/05/2022 05:29:56 JC ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CursoUsuario](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdUser] [int] NULL,
	[IdCurso] [int] NULL,
	[Estado] [bit] NULL,
	[Pagado] [bit] NULL,
 CONSTRAINT [PK_CursoUsuario_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Modulo]    Script Date: 31/05/2022 05:29:56 JC ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Modulo](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdCurso] [int] NULL,
	[Nombre] [nvarchar](200) NULL,
 CONSTRAINT [PK_Seccion] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Progreso]    Script Date: 31/05/2022 05:29:56 JC ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Progreso](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdUser] [int] NOT NULL,
	[progress] [bit] NULL,
	[IdCurso] [int] NULL,
	[IdClase] [int] NULL,
 CONSTRAINT [PK_Progreso] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Recursos]    Script Date: 31/05/2022 05:29:56 JC ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Recursos](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdClase] [int] NULL,
	[Archivo] [nvarchar](100) NULL,
 CONSTRAINT [PK_Recursos] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Requisitos]    Script Date: 31/05/2022 05:29:56 JC ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Requisitos](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[IdCurso] [int] NULL,
	[Requisito] [nvarchar](500) NULL,
 CONSTRAINT [PK_Requisitos] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Roles]    Script Date: 31/05/2022 05:29:56 JC ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roles](
	[Id] [int] NOT NULL,
	[Nombre] [nvarchar](50) NULL,
 CONSTRAINT [PK_DetalleCurso] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 31/05/2022 05:29:56 JC ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuario](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Imagen] [nvarchar](100) NULL,
	[Nombres] [nvarchar](50) NULL,
	[Apellidos] [nvarchar](50) NULL,
	[Celular] [nvarchar](12) NULL,
	[Correo] [nvarchar](50) NULL,
	[Username] [nvarchar](10) NULL,
	[Password] [nvarchar](100) NULL,
	[Recovery] [nvarchar](100) NULL,
	[IdRol] [int] NULL,
	[Twitter] [nvarchar](500) NULL,
	[Facebook] [nvarchar](500) NULL,
	[LinkedIn] [nvarchar](500) NULL,
	[YouTube] [nvarchar](500) NULL,
	[Instagram] [nvarchar](500) NULL,
	[Titulo] [nvarchar](75) NULL,
	[Biografia] [nvarchar](2000) NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Aprendizaje] ON 

INSERT [dbo].[Aprendizaje] ([Id], [IdCurso], [Descripcion]) VALUES (1, 6, N'Angular CLI')
INSERT [dbo].[Aprendizaje] ([Id], [IdCurso], [Descripcion]) VALUES (2, 6, N'Firebase   ')
INSERT [dbo].[Aprendizaje] ([Id], [IdCurso], [Descripcion]) VALUES (3, 6, N'Firebase RESTful services')
INSERT [dbo].[Aprendizaje] ([Id], [IdCurso], [Descripcion]) VALUES (4, 6, N'CRUD')
INSERT [dbo].[Aprendizaje] ([Id], [IdCurso], [Descripcion]) VALUES (5, 6, N'Firebase Cloud Functions')
INSERT [dbo].[Aprendizaje] ([Id], [IdCurso], [Descripcion]) VALUES (6, 6, N'TypeScript   ')
INSERT [dbo].[Aprendizaje] ([Id], [IdCurso], [Descripcion]) VALUES (7, 7, N'Publica tus aplicaciones en Google Play')
INSERT [dbo].[Aprendizaje] ([Id], [IdCurso], [Descripcion]) VALUES (8, 5, N'Crear variables de tipo cadena, numérico y booleanos.')
INSERT [dbo].[Aprendizaje] ([Id], [IdCurso], [Descripcion]) VALUES (9, 5, N'Crear estructuras condicionales y ciclos como for, while o do... while')
INSERT [dbo].[Aprendizaje] ([Id], [IdCurso], [Descripcion]) VALUES (10, 5, N'Modificarar las cadenas y subcadenas, así como leerá los caracteres de una subcadena, así como crear funciones propias.')
INSERT [dbo].[Aprendizaje] ([Id], [IdCurso], [Descripcion]) VALUES (11, 5, N'Crear, poblar y recorrer objetos y arreglos, así como el manejo de fechas.')
INSERT [dbo].[Aprendizaje] ([Id], [IdCurso], [Descripcion]) VALUES (12, 5, N'Uso del HTML5 para crear páginas, crear formularios y utilizar los APIS con JavaScript.')
INSERT [dbo].[Aprendizaje] ([Id], [IdCurso], [Descripcion]) VALUES (13, 5, N'Manejo del CANVAS, desde dibujar hasta hacer una sencilla aplicación.')
INSERT [dbo].[Aprendizaje] ([Id], [IdCurso], [Descripcion]) VALUES (14, 5, N'Crear páginas web dinámicas con JavaScript, HTML5 y CSS3.')
INSERT [dbo].[Aprendizaje] ([Id], [IdCurso], [Descripcion]) VALUES (15, 11, N'Crear variables de tipo cadena, numérico y booleanos.')
SET IDENTITY_INSERT [dbo].[Aprendizaje] OFF
GO
SET IDENTITY_INSERT [dbo].[Categoria] ON 

INSERT [dbo].[Categoria] ([Id], [Nombre]) VALUES (1, N'Desarrollo Web')
INSERT [dbo].[Categoria] ([Id], [Nombre]) VALUES (4, N'Desarrollo Movil')
INSERT [dbo].[Categoria] ([Id], [Nombre]) VALUES (7, N'Desarrollo de Videojuegos')
SET IDENTITY_INSERT [dbo].[Categoria] OFF
GO
SET IDENTITY_INSERT [dbo].[Cesta] ON 

INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (1, 4, 3, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (2, 2, 6, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (3, 2, 7, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (4, 5, 6, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (5, 3, 6, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (6, 1007, 5, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (7, 1007, 6, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (8, 1006, 6, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (9, 1006, 5, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (10, 2, 8, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (11, 2, 9, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (13, 1006, 8, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (14, 3, 7, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (15, 5, 5, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (16, 1007, 7, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (17, 1, 7, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (18, 1, 5, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (19, 1009, 6, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (20, 3, 5, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (21, 2, 10, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (22, 1010, 5, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (23, 2, 11, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (24, 2, 5, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (25, 4, 12, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (26, 4, 5, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (27, 1011, 6, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (28, 2, 13, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (29, 1011, 5, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (30, 1011, 13, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (31, 1011, 7, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (32, 1011, 11, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (33, 1011, 12, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (34, 4, 14, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (35, 1011, 8, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (36, 1011, 9, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (37, 1013, 15, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (38, 3, 13, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (39, 1013, 5, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (41, 1, 11, 1)
INSERT [dbo].[Cesta] ([Id], [IdUser], [IdCurso], [Pagado]) VALUES (42, 1013, 7, 1)
SET IDENTITY_INSERT [dbo].[Cesta] OFF
GO
SET IDENTITY_INSERT [dbo].[Clases] ON 

INSERT [dbo].[Clases] ([Id], [IdModulo], [Nombre], [Video], [Descripcion]) VALUES (1, 1, N'Ejemplo2', N'/video/1.mp4', N'La clase trata de estop')
INSERT [dbo].[Clases] ([Id], [IdModulo], [Nombre], [Video], [Descripcion]) VALUES (2, 2, N'Temas previos', N'/video/1.mp4', N'Veremos los temas que se tratarán a lo largo del curso')
INSERT [dbo].[Clases] ([Id], [IdModulo], [Nombre], [Video], [Descripcion]) VALUES (3, 3, N'Comentarios', N'https://www.youtube.com/embed/1B1U6zQXJTM', N'Veremos los temas que se tratarán a lo largo del curso, comentarios')
INSERT [dbo].[Clases] ([Id], [IdModulo], [Nombre], [Video], [Descripcion]) VALUES (1002, 1002, N'Historia de Internet', N'https://www.youtube.com/embed/ivdTnPl1ND0', N'El código de programa de JavaScript, llamado script, se introduce directamente en el documento HTML y no necesita ser compilado. El navegador se encarga de traducir dicho código. JavaScript fue creado por Brendan Eich en 1995 para Netscape bajo el nombre de “Mocha”.')
INSERT [dbo].[Clases] ([Id], [IdModulo], [Nombre], [Video], [Descripcion]) VALUES (1003, 1002, N'JavaScript en un archivo js', N'https://www.youtube.com/embed/C5FXZ2ki13k', N'Las páginas de internet modernas deben estar divididas por archivo: el archivo html guardará los contenidos, el archivo CSS los estilos y formato, y toda la lógica de programación deberá estar almacenada en archivos con extensión js. Así, si desea modificar la lógica, no afectará a las otras dos, y visceversa.')
INSERT [dbo].[Clases] ([Id], [IdModulo], [Nombre], [Video], [Descripcion]) VALUES (1004, 1004, N'Introducción a las variables en JavaScript', N'https://www.youtube.com/embed/tmRa0_EvMoc', N'JavaScript admite prácticamente cualquier tipo de nombre para definir una variable, no obstante, hay una serie de consideraciones que se deben tener presentes:  El primer      carácter debe ser siempr una letra o el guión de subrayado ( _ ).  Los restantes caracteres pueden ser      letras, números o el guión de subrayado, teniendo como precaución no dejar      espacios entre ellos.  El nombre de la variable no debe coincidir las palabras reservadas de JavaScript.  JavaScript diferencia entre mayúsculas y minúsculas.')
INSERT [dbo].[Clases] ([Id], [IdModulo], [Nombre], [Video], [Descripcion]) VALUES (1005, 1004, N'Tipos de datos en JavaScript', N'https://www.youtube.com/embed/RqQ1d1qEWlE', N'JavaScript puede manejar tres tipos de datos distintos decidiendo por nosotros el tipo de variable que deberá emplear durante la ejecución del script.')
INSERT [dbo].[Clases] ([Id], [IdModulo], [Nombre], [Video], [Descripcion]) VALUES (1006, 1005, N'Sentencias Condicionales', N'https://www.youtube.com/embed/54Oi-HaMZ7E', N'En determinados momentos es necesario controlar el desarrollo del programa para que éste tome decisiones por nosotros.  JavaScript dispone de los siguientes comandos:  if, for, while, do while')
INSERT [dbo].[Clases] ([Id], [IdModulo], [Nombre], [Video], [Descripcion]) VALUES (1007, 1006, N'El ciclo while', N'https://www.youtube.com/embed/54Oi-HaMZ7E', N'La instrucción while ejecuta de manera constante una determinada secuencia de código siempre que se cumpla una condición.  Este tipo de bucles puede generar secuencias infinitas, por lo que se debe tener cuidado cuando se definen los bucles.')
INSERT [dbo].[Clases] ([Id], [IdModulo], [Nombre], [Video], [Descripcion]) VALUES (1008, 1007, N'Funciones de JavaScript', N'https://www.youtube.com/embed/tmRa0_EvMoc', N'Las funciones son uno de los pilares en los que se basa JavaScript.  Una función es un conjunto de sentencias JavaScript que realizan alguna tarea específica.  Las partes que definen una función son:  El nombre de la función.  La lista de argumentos de la función, encerrados entre paréntesis y separados por      comas(“,”)  Las sentencias en JavaScript que definen la función, encerradas entre llaves({,});')
INSERT [dbo].[Clases] ([Id], [IdModulo], [Nombre], [Video], [Descripcion]) VALUES (1009, 1008, N'Concatenación de cadenas y conversión de datos', N'https://www.youtube.com/embed/RqQ1d1qEWlE', N'El proceso de “juntar” cadenas la conoceremos como “concatenación”. En JavaScript, para juntar subcadenas en una cadena la realizaremos con el operado más (+). En HTML todos los campos son una cadena.  Realmente no hay valores numéricos, ni fechas, solo cadenas.')
INSERT [dbo].[Clases] ([Id], [IdModulo], [Nombre], [Video], [Descripcion]) VALUES (1010, 1009, N'Introducción a la sesión', N'https://www.youtube.com/embed/1B1U6zQXJTM', NULL)
INSERT [dbo].[Clases] ([Id], [IdModulo], [Nombre], [Video], [Descripcion]) VALUES (1011, 1009, N'Demostración de TypeScript', N'https://www.youtube.com/embed/1B1U6zQXJTM', N'Un código ilustrativo de las bondades del TypeScript')
INSERT [dbo].[Clases] ([Id], [IdModulo], [Nombre], [Video], [Descripcion]) VALUES (1012, 1009, N'Configuración de TypeScript', N'https://www.youtube.com/embed/1B1U6zQXJTM', N'Inicializando el proyecto de TypeScript')
INSERT [dbo].[Clases] ([Id], [IdModulo], [Nombre], [Video], [Descripcion]) VALUES (1013, 1010, N'Introducción a la sesión', N'https://www.youtube.com/embed/1B1U6zQXJTM', NULL)
INSERT [dbo].[Clases] ([Id], [IdModulo], [Nombre], [Video], [Descripcion]) VALUES (1014, 1011, N'Introducción a la sesión', N'https://www.youtube.com/embed/1B1U6zQXJTM', NULL)
INSERT [dbo].[Clases] ([Id], [IdModulo], [Nombre], [Video], [Descripcion]) VALUES (1015, 1012, N'Pipe: Slice', N'https://www.youtube.com/embed/1B1U6zQXJTM', N'Un pipe que sirve para cortar elementos, arreglos o strings.')
INSERT [dbo].[Clases] ([Id], [IdModulo], [Nombre], [Video], [Descripcion]) VALUES (1016, 1013, N'Introducción a la sesión', N'https://www.youtube.com/embed/RqQ1d1qEWlE', N'JavaScript admite prácticamente cualquier tipo de nombre para definir una variable, no obstante, hay una serie de consideraciones que se deben tener presentes:  El primer      carácter debe ser siempr una letra o el guión de subrayado ( _ ).  Los restantes caracteres pueden ser      letras, números o el guión de subrayado, teniendo como precaución no dejar      espacios entre ellos.  El nombre de la variable no debe coincidir las palabras reservadas de JavaScript.  JavaScript diferencia entre mayúsculas y minúsculas.')
INSERT [dbo].[Clases] ([Id], [IdModulo], [Nombre], [Video], [Descripcion]) VALUES (1017, 1014, N'Tipos de datos en c#', N'/video/1.mp4', N'Veremos los temas que se tratarán a lo largo del curso, comentarios')
INSERT [dbo].[Clases] ([Id], [IdModulo], [Nombre], [Video], [Descripcion]) VALUES (1018, 1014, N'Temas previos', N'https://youtu.be/vdX4U6A1RZ8', N'El código de programa de JavaScript, llamado script, se introduce directamente en el documento HTML y no necesita ser compilado. El navegador se encarga de traducir dicho código. JavaScript fue creado por Brendan Eich en 1995 para Netscape bajo el nombre de “Mocha”.')
INSERT [dbo].[Clases] ([Id], [IdModulo], [Nombre], [Video], [Descripcion]) VALUES (1019, 1015, N'Tipos de datos en JavaScript', N'https://youtu.be/6W2wYwHQNT4', NULL)
SET IDENTITY_INSERT [dbo].[Clases] OFF
GO
SET IDENTITY_INSERT [dbo].[ComentarioCurso] ON 

INSERT [dbo].[ComentarioCurso] ([Id], [IdCurso], [IdUsuario], [Comentario]) VALUES (1, 1, 3, N'El mejor curso de programación web que he llevado. ¡Les recomiendo!')
INSERT [dbo].[ComentarioCurso] ([Id], [IdCurso], [IdUsuario], [Comentario]) VALUES (5, 6, 3, N'Todo muy bien explicado. Estaría bien meterle más caña a C# y actualizar el capitulo de iluminación.')
INSERT [dbo].[ComentarioCurso] ([Id], [IdCurso], [IdUsuario], [Comentario]) VALUES (7, 7, 5, N'Las clases son interactivas, el docente explica bien el curso y el material de apoyo es excelente. ¡Recomendado!')
INSERT [dbo].[ComentarioCurso] ([Id], [IdCurso], [IdUsuario], [Comentario]) VALUES (1005, 6, 1006, N'En general muy buen curso, cubre la base de las herramientas.')
INSERT [dbo].[ComentarioCurso] ([Id], [IdCurso], [IdUsuario], [Comentario]) VALUES (1006, 8, 1006, N'Las clases son interactivas, el docente explica bien el curso y el material de apoyo es excelente. ¡Recomendado!')
INSERT [dbo].[ComentarioCurso] ([Id], [IdCurso], [IdUsuario], [Comentario]) VALUES (1007, 6, 1008, N'Un curso muy completo, falta actualizar las ultimas lecciones a la version mas reciente, pero un muy buen curso.')
INSERT [dbo].[ComentarioCurso] ([Id], [IdCurso], [IdUsuario], [Comentario]) VALUES (1008, 6, 1008, N'Me gusto mucho por que es demasiado puntual me parece excelente.')
SET IDENTITY_INSERT [dbo].[ComentarioCurso] OFF
GO
SET IDENTITY_INSERT [dbo].[Curso] ON 

INSERT [dbo].[Curso] ([Id], [Imagen], [Nombre], [Detalle], [Descripcion], [Precio], [IdDocente], [IdCategoria], [Video], [Estado]) VALUES (5, N'/FilesBD/js2.jpg', N'JavaScript', N'Las principales empresas ofrecen este curso a sus empleados.Este curso fue seleccionado para nuestra colección de cursos mejor calificados en los que confían empresas de todo el mundo.', N'Aprende el lenguaje más popular! JavaScript!

El curso incluye la Sintaxis de ECMAScript ES6, ES7, ES8, ES9, ES10, ES11 y ES12

También el curso incluye Node.js, Express, Mongodb, React  y mucho más!

En este curso aprenderás JavaScript desde los fundamentos hasta temas más avanzados como Prototypes, Delegation, Classes, Ajax, Promises, Generadores, Orientado a Objetos, Fetch API, Async Await, Async JS, Objetos, así como consumir REST APIs, API''s de JavaScript nátivas como Notification API, Speech API y mucho más!', CAST(0.00 AS Decimal(18, 2)), 4, 1, N'/video/1.mp4', NULL)
INSERT [dbo].[Curso] ([Id], [Imagen], [Nombre], [Detalle], [Descripcion], [Precio], [IdDocente], [IdCategoria], [Video], [Estado]) VALUES (6, N'/FilesBD/course-2.jpg', N'Angular', N'Las principales empresas ofrecen este curso a sus empleados.
Este curso fue seleccionado para nuestra colección de cursos mejor calificados en los que confían empresas de todo el mundo.', N'Angular es un marco de trabajo (framework) de front-end impulsado por Google. Creado para desarrollar aplicaciones web, aplicaciones móviles o realizar procesos del lado del servidor utilizando NodeJS.

Este curso se enfoca en llevarte de la mano desde cero hasta poder crear aplicaciones de todo tipo, que van desde páginas web de una sola página (SPA - single page application) hasta conectarnos a una base de datos para realizar proceso de inserción, actualización, eliminación y selección de información. Este curso contiene todo lo que tu necesitas para poder crear aplicaciones con este framework tan potente y veloz.', CAST(30.00 AS Decimal(18, 2)), 2, 1, N'/video/1.mp4', NULL)
INSERT [dbo].[Curso] ([Id], [Imagen], [Nombre], [Detalle], [Descripcion], [Precio], [IdDocente], [IdCategoria], [Video], [Estado]) VALUES (7, N'/FilesBD/course-1.jpg', N'Desarrollo en Android', N'Las principales empresas ofrecen este curso a sus empleados.
Este curso fue seleccionado para nuestra colección de cursos mejor calificados en los que confían empresas de todo el mundo.', N'Crearás desde las aplicaciones más básicas hasta las más sofisticadas multipantalla, con utilización de sensores, gestión de datos y conexión a Internet...y ahora también enseñamos cómo funciona el Internet de las Cosas y conectamos con la tarjeta Arduino/Genuino 101 haciendo uso de la última tecnología Bluetooth Low Energy.', CAST(0.00 AS Decimal(18, 2)), 7, 4, N'/video/1.mp4', NULL)
INSERT [dbo].[Curso] ([Id], [Imagen], [Nombre], [Detalle], [Descripcion], [Precio], [IdDocente], [IdCategoria], [Video], [Estado]) VALUES (8, N'/FilesBD/course-3.jpg', N'Unity', N'Edición de vídeos', N'Edición de vídeos y material didáctico', CAST(20.00 AS Decimal(18, 2)), 7, 7, N'/video/1.mp4', NULL)
INSERT [dbo].[Curso] ([Id], [Imagen], [Nombre], [Detalle], [Descripcion], [Precio], [IdDocente], [IdCategoria], [Video], [Estado]) VALUES (10, N'/FilesBD/react-js.png', N'React JS', N'Las principales empresas ofrecen este curso a sus empleados. Este curso fue seleccionado para nuestra colección de cursos mejor calificados en los que confían empresas de todo el mundo.', N'En este curso vamos a aprender a utilizar de forma sencilla y rápida la libreria para el desarrollo de frontend creada por Facebook llamada React JS. utilizada hoy en día en el desarrollo de sitios de grandes empresas tales como Dropbox, ebay, Instagram, PayPal y Netflix.

Completamente desde 0, iniciando con las bases esenciales para dominar la creacion y renderizado de componentes. Importante también los eventos de cada uno de los elementos, crear formularios, controlar estados, propiedades, referencias, comunicación entre componentes, ruteo, hasta construir tus propios proyectos utilizando las mejores prácticas en código para tener implementaciones óptima y escalables. En la primera parte del curso (Actualizado en el 2021) aprenderemos a usar Context.API para el manejo global de la información en tus componentes, que también puede fungir como un state management. En el ejemplo vamos a llamar a una REST API utilizando Fetch.

El objetivo principal es que aprendas las bases sólidas de la librería y aprendas a crear una estructura óptima para llevar a cabo cualquier tipo de proyecto de React Js hacia el éxito. He puesto todo mi esfuerzo para poder transmitirte mi experiencia durante más de 6 años usando esta librería.', CAST(0.00 AS Decimal(18, 2)), 6, 1, N'https://youtu.be/c79C_S0E4IQ', NULL)
INSERT [dbo].[Curso] ([Id], [Imagen], [Nombre], [Detalle], [Descripcion], [Precio], [IdDocente], [IdCategoria], [Video], [Estado]) VALUES (11, N'/FilesBD/course-1.jpg', N'C#', N'Las principales empresas ofrecen este curso a sus empleados. Este curso fue seleccionado para nuestra colección de cursos mejor calificados en los que confían empresas de todo el mundo.', N'Las principales empresas ofrecen este curso a sus empleados. Este curso fue seleccionado para nuestra colección de cursos mejor calificados en los que confían empresas de todo el mundo.Las principales empresas ofrecen este curso a sus empleados. Este curso fue seleccionado para nuestra colección de cursos mejor calificados en los que confían empresas de todo el mundo.Las principales empresas ofrecen este curso a sus empleados. Este curso fue seleccionado para nuestra colección de cursos mejor calificados en los que confían empresas de todo el mundo.Las principales empresas ofrecen este curso a sus empleados. Este curso fue seleccionado para nuestra colección de cursos mejor calificados en los que confían empresas de todo el mundo.Las principales empresas ofrecen este curso a sus empleados. Este curso fue seleccionado para nuestra colección de cursos mejor calificados en los que confían empresas de todo el mundo.Las principales empresas ofrecen este curso a sus empleados. Este curso fue seleccionado para nuestra colección de cursos mejor calificados en los que confían empresas de todo el mundo.Las principales empresas ofrecen este curso a sus empleados. Este curso fue seleccionado para nuestra colección de cursos mejor calificados en los que confían empresas de todo el mundo.Las principales empresas ofrecen este curso a sus empleados. Este curso fue seleccionado para nuestra colección de cursos mejor calificados en los que confían empresas de todo el mundo.Las principales empresas ofrecen este curso a sus empleados. Este curso fue seleccionado para nuestra colección de cursos mejor calificados en los que confían empresas de todo el mundo.', CAST(0.00 AS Decimal(18, 2)), 4, 1, N'https://youtu.be/c79C_S0E4IQ', NULL)
INSERT [dbo].[Curso] ([Id], [Imagen], [Nombre], [Detalle], [Descripcion], [Precio], [IdDocente], [IdCategoria], [Video], [Estado]) VALUES (13, N'/FilesBD/cat-3.jpg', N'Git', N'Git es un sistema de control de versiones distribuido , mediante el cual podemos trabajar a nivel local sin tener que comprometer el repositorio principal a cada cambio que realicemos', N'Este curso te llevará de la mano partiendo desde cero hasta tener más de lo que necesitas para trabajar con Git, adicionalmente aprenderás a utilizar GitHub como un repositorio remoto, incluyendo formas de trabajar en equipo, flujos de trabajo, tokens, Pull Request y otros temas avanzados que serán de mucha utilidad en tu vida profesional.

En los inicios del curso empezaremos desde lo más básico haciendo ejercicios prácticos. Durante el transcurso del curso, trabajaras de forma colectiva, real y aplicando los conocimientos con más de 14 ejercicios y ejemplos.', CAST(15.00 AS Decimal(18, 2)), 2, 1, N'https://www.youtube.com/embed/ANF1X42_ae4', NULL)
INSERT [dbo].[Curso] ([Id], [Imagen], [Nombre], [Detalle], [Descripcion], [Precio], [IdDocente], [IdCategoria], [Video], [Estado]) VALUES (14, N'/FilesBD/lenguaje-html5.jpg', N'HTML 5', N'Bienvenido al Curso de HTML5 completo y desde cero, en el que aprenderemos todo lo necesario para dominar HTML5, crear tus primeras páginas web e iniciarte en el maravilloso mundo del desarrollo web.', N'HTML5 es el lenguaje de marcas de hipertexto más importante de la web ya que todo sitio o aplicación web lo usa para definir la estructura y el contenido de sus páginas.

Estas ante un curso completo, desde cero, con explicaciones super claras, con contenido único y exclusivo, con muchos ejercicios y practicas para aprender en profundidad HTML.', CAST(0.00 AS Decimal(18, 2)), 4, 1, N'https://www.youtube.com/embed/QC9_8nRNNHA', NULL)
SET IDENTITY_INSERT [dbo].[Curso] OFF
GO
SET IDENTITY_INSERT [dbo].[CursoUsuario] ON 

INSERT [dbo].[CursoUsuario] ([Id], [IdUser], [IdCurso], [Estado], [Pagado]) VALUES (1, 2, 1, 1, 0)
SET IDENTITY_INSERT [dbo].[CursoUsuario] OFF
GO
SET IDENTITY_INSERT [dbo].[Modulo] ON 

INSERT [dbo].[Modulo] ([Id], [IdCurso], [Nombre]) VALUES (1, 7, N'Ejemplo')
INSERT [dbo].[Modulo] ([Id], [IdCurso], [Nombre]) VALUES (2, 6, N'Introducción')
INSERT [dbo].[Modulo] ([Id], [IdCurso], [Nombre]) VALUES (3, 6, N'Elementos básicos')
INSERT [dbo].[Modulo] ([Id], [IdCurso], [Nombre]) VALUES (1002, 5, N'JavaScript: Sintaxis del lenguaje')
INSERT [dbo].[Modulo] ([Id], [IdCurso], [Nombre]) VALUES (1004, 5, N'JavaScript: Variables y tipos de datos')
INSERT [dbo].[Modulo] ([Id], [IdCurso], [Nombre]) VALUES (1005, 5, N'JavaScript: Sentencias condicionales')
INSERT [dbo].[Modulo] ([Id], [IdCurso], [Nombre]) VALUES (1006, 5, N'JavaScript: Ciclos')
INSERT [dbo].[Modulo] ([Id], [IdCurso], [Nombre]) VALUES (1007, 5, N'JavaScript: Funciones')
INSERT [dbo].[Modulo] ([Id], [IdCurso], [Nombre]) VALUES (1008, 5, N'JavaScript: Manejo de cadenas')
INSERT [dbo].[Modulo] ([Id], [IdCurso], [Nombre]) VALUES (1009, 6, N'Gentil introducción a TypeScript y ES6')
INSERT [dbo].[Modulo] ([Id], [IdCurso], [Nombre]) VALUES (1010, 6, N'Aplicación #1: Hola Mundo')
INSERT [dbo].[Modulo] ([Id], [IdCurso], [Nombre]) VALUES (1011, 6, N'Aplicación #1: Aplicación de una sola página (SPA)')
INSERT [dbo].[Modulo] ([Id], [IdCurso], [Nombre]) VALUES (1012, 6, N'Pipes - Transforman los valores mostrados en pantalla')
INSERT [dbo].[Modulo] ([Id], [IdCurso], [Nombre]) VALUES (1013, 11, N'Introducción')
INSERT [dbo].[Modulo] ([Id], [IdCurso], [Nombre]) VALUES (1014, 11, N'Elementos básicos')
INSERT [dbo].[Modulo] ([Id], [IdCurso], [Nombre]) VALUES (1015, 11, N'JavaScript: Sintaxis del lenguaje')
SET IDENTITY_INSERT [dbo].[Modulo] OFF
GO
SET IDENTITY_INSERT [dbo].[Progreso] ON 

INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (1, 1011, 1, 10, 1002)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (2, 1011, 1, 10, 1003)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (3, 1011, 1, 10, 1004)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (4, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (5, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (6, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (7, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (8, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (9, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (10, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (11, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (12, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (13, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (14, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (15, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (16, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (17, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (18, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (19, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (20, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (21, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (22, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (23, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (24, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (25, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (26, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (27, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (28, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (29, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (30, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (31, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (32, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (33, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (34, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (35, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (36, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (37, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (38, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (39, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (40, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (41, 1011, 1, 11, 0)
INSERT [dbo].[Progreso] ([Id], [IdUser], [progress], [IdCurso], [IdClase]) VALUES (42, 1011, 1, 11, 0)
SET IDENTITY_INSERT [dbo].[Progreso] OFF
GO
SET IDENTITY_INSERT [dbo].[Recursos] ON 

INSERT [dbo].[Recursos] ([Id], [IdClase], [Archivo]) VALUES (1, 1, N'/Recursos/EDT - CASO RIESGO IMPL.pdf')
INSERT [dbo].[Recursos] ([Id], [IdClase], [Archivo]) VALUES (2, 1, N'/Recursos/Matriz de Adquisiciones - CASO RIESGO.pdf')
INSERT [dbo].[Recursos] ([Id], [IdClase], [Archivo]) VALUES (3, 1, N'/Recursos/Matriz de Costos  - CASO RIESGO IMPL.pdf')
INSERT [dbo].[Recursos] ([Id], [IdClase], [Archivo]) VALUES (4, 2, N'/Recursos/Registro de Riesgos  - CASO RIESGO.pdf')
INSERT [dbo].[Recursos] ([Id], [IdClase], [Archivo]) VALUES (5, 3, N'/Recursos/EDT - CASO RIESGO IMPL.pdf')
INSERT [dbo].[Recursos] ([Id], [IdClase], [Archivo]) VALUES (1002, 1002, N'/Recursos/manual-javascript.pdf')
INSERT [dbo].[Recursos] ([Id], [IdClase], [Archivo]) VALUES (1003, 1003, N'/Recursos/manual-javascript.pdf')
INSERT [dbo].[Recursos] ([Id], [IdClase], [Archivo]) VALUES (1004, 1005, N'/Recursos/manual-javascript.pdf')
INSERT [dbo].[Recursos] ([Id], [IdClase], [Archivo]) VALUES (1005, 1008, N'/Recursos/manual-javascript.pdf')
INSERT [dbo].[Recursos] ([Id], [IdClase], [Archivo]) VALUES (1006, 1009, N'/Recursos/manual-javascript.pdf')
INSERT [dbo].[Recursos] ([Id], [IdClase], [Archivo]) VALUES (1007, 1010, N'/Recursos/angularjs-es.18.pdf')
INSERT [dbo].[Recursos] ([Id], [IdClase], [Archivo]) VALUES (1008, 1012, N'/Recursos/angularjs-es.18.pdf')
INSERT [dbo].[Recursos] ([Id], [IdClase], [Archivo]) VALUES (1009, 1014, N'/Recursos/angularjs-es.18.pdf')
INSERT [dbo].[Recursos] ([Id], [IdClase], [Archivo]) VALUES (1010, 1016, N'/Recursos/angularjs-es.18.pdf')
INSERT [dbo].[Recursos] ([Id], [IdClase], [Archivo]) VALUES (1011, 1017, N'/Recursos/Proceso de llevar un curso.png')
INSERT [dbo].[Recursos] ([Id], [IdClase], [Archivo]) VALUES (1012, 1019, N'/Recursos/manual-javascript.pdf')
SET IDENTITY_INSERT [dbo].[Recursos] OFF
GO
SET IDENTITY_INSERT [dbo].[Requisitos] ON 

INSERT [dbo].[Requisitos] ([Id], [IdCurso], [Requisito]) VALUES (1, 6, N'Conocimiento de JavaScript básico es necesario')
INSERT [dbo].[Requisitos] ([Id], [IdCurso], [Requisito]) VALUES (2, 6, N'Conocimiento básico de la estructura de páginas HTML')
INSERT [dbo].[Requisitos] ([Id], [IdCurso], [Requisito]) VALUES (3, 6, N'NO es necesario conocimiento de Angular 1')
INSERT [dbo].[Requisitos] ([Id], [IdCurso], [Requisito]) VALUES (4, 6, N'NO es necesario conocimiento de TypeScript')
INSERT [dbo].[Requisitos] ([Id], [IdCurso], [Requisito]) VALUES (5, 6, N'NO es necesario conocimiento alguno sobre Angular o Ionic')
INSERT [dbo].[Requisitos] ([Id], [IdCurso], [Requisito]) VALUES (6, 6, N'NO es necesario conocimiento de ES6, ECMAScript 6 o ES2015')
INSERT [dbo].[Requisitos] ([Id], [IdCurso], [Requisito]) VALUES (7, 7, N'Ordenador con navegador y conexión a Internet')
INSERT [dbo].[Requisitos] ([Id], [IdCurso], [Requisito]) VALUES (8, 5, N'Conocimientos generales de páginas web y computación')
INSERT [dbo].[Requisitos] ([Id], [IdCurso], [Requisito]) VALUES (9, 5, N'Conocimientos generales de páginas web y computación')
INSERT [dbo].[Requisitos] ([Id], [IdCurso], [Requisito]) VALUES (10, 11, N'Conocimientos generales de computación')
SET IDENTITY_INSERT [dbo].[Requisitos] OFF
GO
INSERT [dbo].[Roles] ([Id], [Nombre]) VALUES (1, N'Administrador')
INSERT [dbo].[Roles] ([Id], [Nombre]) VALUES (2, N'Docente')
INSERT [dbo].[Roles] ([Id], [Nombre]) VALUES (3, N'Estudiante')
GO
SET IDENTITY_INSERT [dbo].[Usuario] ON 

INSERT [dbo].[Usuario] ([Id], [Imagen], [Nombres], [Apellidos], [Celular], [Correo], [Username], [Password], [Recovery], [IdRol], [Twitter], [Facebook], [LinkedIn], [YouTube], [Instagram], [Titulo], [Biografia]) VALUES (1, N'/img/Jhan.jpeg', N'Jhan', N'Ignacio', NULL, N'juliaRAF2020@gmail.com', N'JhanAdmin', N'F+gLTtJi4gxG8d9vbgprZ5MyV5nssE5YhCtiR54gWI3OPZ2BR0LMMHe8IeW0h4jEiyruv35Sloeqc96JfW1rNA==', NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Usuario] ([Id], [Imagen], [Nombres], [Apellidos], [Celular], [Correo], [Username], [Password], [Recovery], [IdRol], [Twitter], [Facebook], [LinkedIn], [YouTube], [Instagram], [Titulo], [Biografia]) VALUES (2, N'/img/team-4.jpg', N'Katty', N'Valiente Saldaña', N'910789456', N'katherine@gmail.com', N'katty31', N'F+gLTtJi4gxG8d9vbgprZ5MyV5nssE5YhCtiR54gWI3OPZ2BR0LMMHe8IeW0h4jEiyruv35Sloeqc96JfW1rNA==', NULL, 2, NULL, N'Null', NULL, N'Null', NULL, N'Lic. Computación', N'La profesora Katty Valiente nació en la ciudad de Zacatecas, el 16 de octubre de 1890. Se recibió como profesor de Enseñanza Elemental en 1909 en el Instituto de Ciencias de su Estado natal.
De 1909 - 1913 fue profesor de grupo en escuelas primarias de Zacatecas donde llegó a ser inspector.
En 1914 fue profesor de la Escuela Primaria "Horacio Mann" en la Ciudad de México, prestando posteriormente sus servicios como profesor en la Escuela Primaria Anexa a la Normal de varones.
En 1925 se le distinguió como maestro al ser enviado a estudiar a Nueva York, estudios que le merecieron ser designado profesor de Técnica de la Enseñanza en la Escuela Nacional de Maestros.
Su actuación de maestro se extendió a otras instituciones educativas; fue Secretario de la Escuela Normal Nocturna; también fue maestro de Matemáticas en la entonces Escuela Práctica de Mecánicos Eléctricos y llegó a ocupar el cargo de Subdirector de la Institución.')
INSERT [dbo].[Usuario] ([Id], [Imagen], [Nombres], [Apellidos], [Celular], [Correo], [Username], [Password], [Recovery], [IdRol], [Twitter], [Facebook], [LinkedIn], [YouTube], [Instagram], [Titulo], [Biografia]) VALUES (3, N'/img/testimonial-1.jpg', N'Carla', N'Moreno Calzado', NULL, N'carla24@hotmail.com', N'Carla2022', N'F+gLTtJi4gxG8d9vbgprZ5MyV5nssE5YhCtiR54gWI3OPZ2BR0LMMHe8IeW0h4jEiyruv35Sloeqc96JfW1rNA==', N'T1fCR5BC42KR2v4PZ8sDriQ5uxo1U9j01DFPACR4B6l8TREsOOKjx57rIn8OyYExCOQhDDX7F00GwmnsSe6yRg==', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Usuario] ([Id], [Imagen], [Nombres], [Apellidos], [Celular], [Correo], [Username], [Password], [Recovery], [IdRol], [Twitter], [Facebook], [LinkedIn], [YouTube], [Instagram], [Titulo], [Biografia]) VALUES (4, N'/img/team-1.jpg', N'Hugo ', N'Bellena Salcedo', N'910123456', N'hugo@hotmail.com', N'hugo2022', N'F+gLTtJi4gxG8d9vbgprZ5MyV5nssE5YhCtiR54gWI3OPZ2BR0LMMHe8IeW0h4jEiyruv35Sloeqc96JfW1rNA==', NULL, 2, NULL, NULL, NULL, NULL, NULL, N'Desarrollador de Software', N'El Profesor Hugo Bellena Salcedo nació en la ciudad de Zacatecas, el 16 de octubre de 1890. Se recibió como profesor de Enseñanza Elemental en 1909 en el Instituto de Ciencias de su Estado natal.
De 1909 - 1913 fue profesor de grupo en escuelas primarias de Zacatecas donde llegó a ser inspector.
En 1914 fue profesor de la Escuela Primaria "Horacio Mann" en la Ciudad de México, prestando posteriormente sus servicios como profesor en la Escuela Primaria Anexa a la Normal de varones.
En 1925 se le distinguió como maestro al ser enviado a estudiar a Nueva York, estudios que le merecieron ser designado profesor de Técnica de la Enseñanza en la Escuela Nacional de Maestros.
Su actuación de maestro se extendió a otras instituciones educativas; fue Secretario de la Escuela Normal Nocturna; también fue maestro de Matemáticas en la entonces Escuela Práctica de Mecánicos Eléctricos y llegó a ocupar el cargo de Subdirector de la Institución.')
INSERT [dbo].[Usuario] ([Id], [Imagen], [Nombres], [Apellidos], [Celular], [Correo], [Username], [Password], [Recovery], [IdRol], [Twitter], [Facebook], [LinkedIn], [YouTube], [Instagram], [Titulo], [Biografia]) VALUES (5, N'/img/testimonial-2.jpg', N'Luis', N'Alcantara', NULL, N'luis2022@hotmail.com', N'luis2022', N'F+gLTtJi4gxG8d9vbgprZ5MyV5nssE5YhCtiR54gWI3OPZ2BR0LMMHe8IeW0h4jEiyruv35Sloeqc96JfW1rNA==', NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Usuario] ([Id], [Imagen], [Nombres], [Apellidos], [Celular], [Correo], [Username], [Password], [Recovery], [IdRol], [Twitter], [Facebook], [LinkedIn], [YouTube], [Instagram], [Titulo], [Biografia]) VALUES (6, N'/proyecto/userperfil4.png', N'Jorge Fernando', N'Malca Salazar', N'976123456', N'jorge@gmail.com', N'JorgeDoc', N'F+gLTtJi4gxG8d9vbgprZ5MyV5nssE5YhCtiR54gWI3OPZ2BR0LMMHe8IeW0h4jEiyruv35Sloeqc96JfW1rNA==', NULL, 2, NULL, NULL, NULL, NULL, NULL, N'Desarrollador Web', N'El profesor Jorge Fernando nació en la ciudad de Zacatecas, el 16 de octubre de 1890. Se recibió como profesor de Enseñanza Elemental en 1909 en el Instituto de Ciencias de su Estado natal.
De 1909 - 1913 fue profesor de grupo en escuelas primarias de Zacatecas donde llegó a ser inspector.
En 1914 fue profesor de la Escuela Primaria "Horacio Mann" en la Ciudad de México, prestando posteriormente sus servicios como profesor en la Escuela Primaria Anexa a la Normal de varones.
En 1925 se le distinguió como maestro al ser enviado a estudiar a Nueva York, estudios que le merecieron ser designado profesor de Técnica de la Enseñanza en la Escuela Nacional de Maestros.
Su actuación de maestro se extendió a otras instituciones educativas; fue Secretario de la Escuela Normal Nocturna; también fue maestro de Matemáticas en la entonces Escuela Práctica de Mecánicos Eléctricos y llegó a ocupar el cargo de Subdirector de la Institución.')
INSERT [dbo].[Usuario] ([Id], [Imagen], [Nombres], [Apellidos], [Celular], [Correo], [Username], [Password], [Recovery], [IdRol], [Twitter], [Facebook], [LinkedIn], [YouTube], [Instagram], [Titulo], [Biografia]) VALUES (7, N'/img/team-2.jpg', N'Mayra Pamela', N'Quiroz Salazar', N'976789456', N'mayra@gmail.com', N'MayraDoc', N'F+gLTtJi4gxG8d9vbgprZ5MyV5nssE5YhCtiR54gWI3OPZ2BR0LMMHe8IeW0h4jEiyruv35Sloeqc96JfW1rNA==', NULL, 2, NULL, NULL, NULL, NULL, NULL, N'Desarrollo de Videojuegos 2D-3D', N'La Profesora Mayra Quiroz Salazar nació en la ciudad de Zacatecas, el 16 de octubre de 1890. Se recibió como profesor de Enseñanza Elemental en 1909 en el Instituto de Ciencias de su Estado natal.
De 1909 - 1913 fue profesor de grupo en escuelas primarias de Zacatecas donde llegó a ser inspector.
En 1914 fue profesor de la Escuela Primaria "Horacio Mann" en la Ciudad de México, prestando posteriormente sus servicios como profesor en la Escuela Primaria Anexa a la Normal de varones.
En 1925 se le distinguió como maestro al ser enviado a estudiar a Nueva York, estudios que le merecieron ser designado profesor de Técnica de la Enseñanza en la Escuela Nacional de Maestros.
Su actuación de maestro se extendió a otras instituciones educativas; fue Secretario de la Escuela Normal Nocturna; también fue maestro de Matemáticas en la entonces Escuela Práctica de Mecánicos Eléctricos y llegó a ocupar el cargo de Subdirector de la Institución.')
INSERT [dbo].[Usuario] ([Id], [Imagen], [Nombres], [Apellidos], [Celular], [Correo], [Username], [Password], [Recovery], [IdRol], [Twitter], [Facebook], [LinkedIn], [YouTube], [Instagram], [Titulo], [Biografia]) VALUES (8, N'/FilesBD/testimonial-3.jpg', N'Jorge', N'Salazar Abanto', NULL, N'jor@gmail.com', N'Jorge2022', N'F+gLTtJi4gxG8d9vbgprZ5MyV5nssE5YhCtiR54gWI3OPZ2BR0LMMHe8IeW0h4jEiyruv35Sloeqc96JfW1rNA==', NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Usuario] ([Id], [Imagen], [Nombres], [Apellidos], [Celular], [Correo], [Username], [Password], [Recovery], [IdRol], [Twitter], [Facebook], [LinkedIn], [YouTube], [Instagram], [Titulo], [Biografia]) VALUES (1006, N'/img/team-3.jpg', N'Kevin', N'Quispe', NULL, N'kevin@gmail.com', N'kevinQ', N'F+gLTtJi4gxG8d9vbgprZ5MyV5nssE5YhCtiR54gWI3OPZ2BR0LMMHe8IeW0h4jEiyruv35Sloeqc96JfW1rNA==', NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Usuario] ([Id], [Imagen], [Nombres], [Apellidos], [Celular], [Correo], [Username], [Password], [Recovery], [IdRol], [Twitter], [Facebook], [LinkedIn], [YouTube], [Instagram], [Titulo], [Biografia]) VALUES (1007, N'/img/testimonial-4.jpg', N'Kellyn', N'Carmona', NULL, N'k@gmail.com', N'kellyn', N'F+gLTtJi4gxG8d9vbgprZ5MyV5nssE5YhCtiR54gWI3OPZ2BR0LMMHe8IeW0h4jEiyruv35Sloeqc96JfW1rNA==', NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Usuario] ([Id], [Imagen], [Nombres], [Apellidos], [Celular], [Correo], [Username], [Password], [Recovery], [IdRol], [Twitter], [Facebook], [LinkedIn], [YouTube], [Instagram], [Titulo], [Biografia]) VALUES (1008, N'/proyecto/userperfil.png', N'Edwin', N'Toledo', NULL, N'tole6@hotmail.com', N'Edwin2022', N'F+gLTtJi4gxG8d9vbgprZ5MyV5nssE5YhCtiR54gWI3OPZ2BR0LMMHe8IeW0h4jEiyruv35Sloeqc96JfW1rNA==', NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Usuario] ([Id], [Imagen], [Nombres], [Apellidos], [Celular], [Correo], [Username], [Password], [Recovery], [IdRol], [Twitter], [Facebook], [LinkedIn], [YouTube], [Instagram], [Titulo], [Biografia]) VALUES (1009, N'/FilesBD/Inclusion.png', N'Pedro', N'Valiente', NULL, N'pedro@hotmail.com', N'Pedro2022', N'F+gLTtJi4gxG8d9vbgprZ5MyV5nssE5YhCtiR54gWI3OPZ2BR0LMMHe8IeW0h4jEiyruv35Sloeqc96JfW1rNA==', NULL, 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Usuario] ([Id], [Imagen], [Nombres], [Apellidos], [Celular], [Correo], [Username], [Password], [Recovery], [IdRol], [Twitter], [Facebook], [LinkedIn], [YouTube], [Instagram], [Titulo], [Biografia]) VALUES (1010, N'/FilesBD/react-js.png', N'Juan', N'Perez', NULL, N'juan@hotmail.com', N'juan22', N'F+gLTtJi4gxG8d9vbgprZ5MyV5nssE5YhCtiR54gWI3OPZ2BR0LMMHe8IeW0h4jEiyruv35Sloeqc96JfW1rNA==', N'YzFuyNxgRnH2cOluiKuIaTEcWwFFPnDMGl64B+ZwyRhBNfXaDFtXlWPcX0ekmbomgnj5jHfYGzeXmLhK+PpKEA==', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Usuario] ([Id], [Imagen], [Nombres], [Apellidos], [Celular], [Correo], [Username], [Password], [Recovery], [IdRol], [Twitter], [Facebook], [LinkedIn], [YouTube], [Instagram], [Titulo], [Biografia]) VALUES (1011, N'/FilesBD/testimonial-3.jpg', N'Matias Valentino', N'Alcantara Mantilla', NULL, N'jhanignacio26@gmail.com', N'Matias', N'F+gLTtJi4gxG8d9vbgprZ5MyV5nssE5YhCtiR54gWI3OPZ2BR0LMMHe8IeW0h4jEiyruv35Sloeqc96JfW1rNA==', N'ldc8WC64b4reE1X29u/iC8D1VWzJQWh4YjTNcR7IwT7gxgLMRocyT8KgGV20Ljkc835/Me/uMwcrywZRZxkFdQ==', 3, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[Usuario] ([Id], [Imagen], [Nombres], [Apellidos], [Celular], [Correo], [Username], [Password], [Recovery], [IdRol], [Twitter], [Facebook], [LinkedIn], [YouTube], [Instagram], [Titulo], [Biografia]) VALUES (1013, N'/proyecto/userperfil4.png', N'Nixon', N'Salazar Montes', N'978456123', N'nix@gmail.com', N'Nixon', N'F+gLTtJi4gxG8d9vbgprZ5MyV5nssE5YhCtiR54gWI3OPZ2BR0LMMHe8IeW0h4jEiyruv35Sloeqc96JfW1rNA==', NULL, 2, NULL, NULL, NULL, NULL, NULL, N'Ing. Sistemas Computacionales', N'Nixon Salazar Montes se graduó como Ingeniero Electrónico en la Universidad Nacional de Rosario (UNR) en 1983, y en 1998 obtuvo el título de PhD in Electrical and Computer Engineering de la University of Newcastle, Australia, por su tesis titulada Asymptotic Analysis of Dynamic System Identification using Rational Orthonormal Bases. Desde 1980 ha ejercido diversos cargos docentes en la Facultad de Ciencias Exactas, Ingeniería y Agrimensura (FCEIA), UNR y en la Universidad Tecnológica Nacional. En el año académico 1987/1988 realizó una estancia de investigación en el Dipartimento di Automatica e Informatica, Politecnico di Torino, Italia, con una beca del gobierno italiano. Entre 1989 y 1994 fue becario de CONICET trabajando en Control No Lineal, y Profesor Adjunto en la FCEIA, UNR. Entre Sept. 2002 y Sept. 2003 realizó una estancia de investigación postdoctoral en la University of Western Ontario, London, Canadá, trabajando en Identificación y Control con Modelo Predictivo No lineal. En la actualidad es Prof. Titular de la FCEIA, UNR. Ha dirigido 7 Tesis de Doctorado ya defendidas y actualmente dirige un becario doctoral y dos becarios posdoctorales con beca de CONICET. Entre las áreas de investigación de su interés pueden mencionarse la Identificación de Sistemas y el Procesamiento de Señales e Imágenes')
SET IDENTITY_INSERT [dbo].[Usuario] OFF
GO
ALTER TABLE [dbo].[Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_Roles] FOREIGN KEY([IdRol])
REFERENCES [dbo].[Roles] ([Id])
GO
ALTER TABLE [dbo].[Usuario] CHECK CONSTRAINT [FK_Usuario_Roles]
GO
USE [master]
GO
ALTER DATABASE [IncVirtual] SET  READ_WRITE 
GO
